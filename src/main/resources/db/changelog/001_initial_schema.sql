set search_path = public;

DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS articles CASCADE;

CREATE TABLE IF NOT EXISTS products
(
    id              BIGSERIAL       NOT NULL,
    title           TEXT            NOT NULL,
    description     TEXT            NOT NULL,
    price           NUMERIC(10, 2)  NOT NULL
);
ALTER TABLE products ADD CONSTRAINT product_pkey PRIMARY KEY (id);

CREATE TABLE IF NOT EXISTS articles
(
    id              BIGSERIAL       NOT NULL,
    title           TEXT            NOT NULL,
    content_text    TEXT            NOT NULL,
    creation_date   TIMESTAMP       NOT NULL,
    product_id      BIGSERIAL       NOT NULL
);
ALTER TABLE articles ADD CONSTRAINT article_pkey PRIMARY KEY (id);
ALTER TABLE articles ADD CONSTRAINT fk_article_product_pkey FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE CASCADE;
