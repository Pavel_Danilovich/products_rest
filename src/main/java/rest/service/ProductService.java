package rest.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import rest.entity.Product;
import rest.repository.ProductRepository;
import rest.service.specifications.ProductSpecification;

@Service
@RequiredArgsConstructor
public class ProductService {
  private final ProductRepository repository;

  public List<Product> findAllProductsSorted(String sort) {
    if (sort != null) {
      if (sort.contains("-")) {
        sort = sort.substring(1);
        return repository.findAll(Sort.by(sort).descending());
      } else {
        return repository.findAll(Sort.by(sort).ascending());
      }
    }
    return repository.findAll();
  }

  public List<Product> findAllProductsFiltered(Map<String, String> filter) {
    if (filter != null) {
      Map.Entry<String, String> filterEntrySet = filter.entrySet().stream()
          .findAny()
          .orElseThrow(() -> new IllegalArgumentException("filter parameter is empty"));

      SearchCriteria searchCriteria;
      if (filterEntrySet.getKey().equals("price")) {
        searchCriteria = getSearchCriteriaForNumbers(filterEntrySet);
      } else {
        searchCriteria = getSearchCriteriaForString(filterEntrySet);
      }
      ProductSpecification productSpecification = new ProductSpecification(searchCriteria);
      return repository.findAll(productSpecification);
    }
    return repository.findAll();
  }

  private SearchCriteria getSearchCriteriaForString(Map.Entry<String, String> filterEntrySet) {
    SearchCriteria criteria = new SearchCriteria();
    criteria.setOperation(FilterOperation.CONTAINING);
    criteria.setKey(filterEntrySet.getKey());
    criteria.setValue(filterEntrySet.getValue());
    return criteria;
  }

  private SearchCriteria getSearchCriteriaForNumbers(Map.Entry<String, String> filterEntrySet) {
    SearchCriteria criteria = new SearchCriteria();
    criteria.setKey(filterEntrySet.getKey());
    if (filterEntrySet.getValue().contains("-")) {
      criteria.setValue(filterEntrySet.getValue().substring(1));
      criteria.setOperation(FilterOperation.LESS_AND_EQUAL);
    } else if (filterEntrySet.getValue().contains(":")) {
      String[] numbers = filterEntrySet.getValue().split(":");
      criteria.setOperation(FilterOperation.BETWEEN);
      criteria.setValue(numbers);
    } else {
      criteria.setValue(filterEntrySet.getValue());
      criteria.setOperation(FilterOperation.GREATER_AND_EQUAL);
    }
    return criteria;
  }

  public Product save(Product product) {
    return repository.save(product);
  }

  public void delete(Product product) {
    repository.deleteById(product.getId());
  }

  public Product update(Product product) {
    repository.findById(product.getId()).orElseThrow(() -> new IllegalArgumentException("Product not found"));
    return repository.save(product);
  }
}
