package rest.service;

import lombok.Data;

@Data
public class SearchCriteria {
  private String key;
  private FilterOperation operation;
  private Object value;
}
