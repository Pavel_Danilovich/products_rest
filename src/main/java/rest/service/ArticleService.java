package rest.service;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import rest.entity.Article;
import rest.repository.ArticleRepository;
import rest.service.specifications.ArticleSpecification;

@Service
@RequiredArgsConstructor
public class ArticleService {
  private final ArticleRepository repository;

  public Article save(Article article) {
    if (article.getCreationDate() == null) {
      article.setCreationDate(Instant.now());
    }
    return repository.save(article);
  }

  public void delete(Article article) {
    repository.deleteById(article.getId());
  }

  public Article update(Article article) {
    repository.findById(article.getId()).orElseThrow(() -> new IllegalArgumentException("Article not found"));
    return repository.save(article);
  }

  public List<Article> findAllProductsSorted(String sort) {
    if (sort != null) {
      if (sort.contains("-")) {
        sort = sort.substring(1);
        return repository.findAll(Sort.by(sort).descending());
      } else {
        return repository.findAll(Sort.by(sort).ascending());
      }
    }
    return repository.findAll();
  }

  public List<Article> findAllProductsFiltered(HashMap<String, String> filter) {
    if (filter != null) {
      Map.Entry<String, String> filterEntrySet = filter.entrySet().stream()
          .findAny()
          .orElseThrow(() -> new IllegalArgumentException("filter parameter is empty"));

      SearchCriteria searchCriteria;
      if (filterEntrySet.getKey().equals("creationDate")) {
        searchCriteria = getSearchCriteriaForInstant(filterEntrySet);
      } else {
        searchCriteria = getSearchCriteriaForString(filterEntrySet);
      }
      ArticleSpecification articleSpecification = new ArticleSpecification(searchCriteria);
      return repository.findAll(articleSpecification);
    }
    return repository.findAll();
  }


  private SearchCriteria getSearchCriteriaForInstant(Map.Entry<String, String> filterEntrySet) {
    SearchCriteria criteria = new SearchCriteria();
    criteria.setKey(filterEntrySet.getKey());
    if (filterEntrySet.getValue().contains("<")) {
      criteria.setValue(filterEntrySet.getValue().substring(1));
      criteria.setOperation(FilterOperation.LESS_AND_EQUAL);
    } else if (filterEntrySet.getValue().contains(">")) {
      criteria.setValue(filterEntrySet.getValue().substring(1));
      criteria.setOperation(FilterOperation.GREATER_AND_EQUAL);
    } else if (filterEntrySet.getValue().contains("::")) {
      String[] instants = filterEntrySet.getValue().split("::");
      criteria.setOperation(FilterOperation.BETWEEN);
      criteria.setValue(instants);
    } else {
      criteria.setValue(filterEntrySet.getValue());
      criteria.setOperation(FilterOperation.GREATER_AND_EQUAL);
    }
    return criteria;
  }

  private SearchCriteria getSearchCriteriaForString(Map.Entry<String, String> filterEntrySet) {
    SearchCriteria criteria = new SearchCriteria();
    criteria.setOperation(FilterOperation.CONTAINING);
    criteria.setKey(filterEntrySet.getKey());
    criteria.setValue(filterEntrySet.getValue());
    return criteria;
  }
}
