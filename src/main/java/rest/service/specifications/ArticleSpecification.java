package rest.service.specifications;

import static rest.service.FilterOperation.GREATER_AND_EQUAL;
import static rest.service.FilterOperation.LESS_AND_EQUAL;


import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import rest.entity.Article;
import rest.entity.Product;
import rest.service.FilterOperation;
import rest.service.SearchCriteria;

public class ArticleSpecification implements Specification<Article> {
  private final SearchCriteria criteria;

  public ArticleSpecification(SearchCriteria criteria) {
    this.criteria = criteria;
  }

  @Override
  public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

    if (criteria.getOperation() == GREATER_AND_EQUAL) {
      return builder.greaterThanOrEqualTo(
          root.<Instant>get(criteria.getKey()), Instant.parse(criteria.getValue().toString()));
    }
    else if (criteria.getOperation() == LESS_AND_EQUAL) {
      return builder.lessThanOrEqualTo(
          root.<Instant>get(criteria.getKey()), Instant.parse(criteria.getValue().toString()));
    }
    else if (criteria.getOperation() == FilterOperation.BETWEEN) {
      Object obj = criteria.getValue();
      List<Instant> instants = castObjectToListOfInstant(obj);

      return builder.between(root.get(criteria.getKey()), instants.get(0), instants.get(1));
    }
    else if (criteria.getOperation() == FilterOperation.CONTAINING) {
      if (root.get(criteria.getKey()).getJavaType() == String.class) {
        return builder.like(
            root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      } else {
        return builder.equal(root.get(criteria.getKey()), criteria.getValue());
      }
    }
    return null;
  }

  private List<Instant> castObjectToListOfInstant(Object obj) {
    List<?> list = new ArrayList<>();
    if (obj.getClass().isArray()) {
      list = Arrays.asList((Object[])obj);
    }
    List<Instant> instants = new ArrayList<>();
    list.forEach(o -> instants.add(Instant.parse(((String) o))));

    return instants;
  }
}
