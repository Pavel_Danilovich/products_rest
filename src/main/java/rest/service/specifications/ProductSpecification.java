package rest.service.specifications;

import static rest.service.FilterOperation.GREATER_AND_EQUAL;
import static rest.service.FilterOperation.LESS_AND_EQUAL;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import rest.entity.Product;
import rest.service.FilterOperation;
import rest.service.SearchCriteria;

public class ProductSpecification implements Specification<Product> {
  private final SearchCriteria criteria;

  public ProductSpecification(SearchCriteria criteria) {
    this.criteria = criteria;
  }

  @Override
  public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

    if (criteria.getOperation() == GREATER_AND_EQUAL) {
      return builder.greaterThanOrEqualTo(
          root.<String>get(criteria.getKey()), criteria.getValue().toString());
    }
    else if (criteria.getOperation() == LESS_AND_EQUAL) {
      return builder.lessThanOrEqualTo(
          root.<String>get(criteria.getKey()), criteria.getValue().toString());
    }
    else if (criteria.getOperation() == FilterOperation.BETWEEN) {
      Object obj = criteria.getValue();
      List<BigDecimal> numbers = castObjectToListOfBigDecimal(obj);

      return builder.between(root.get(criteria.getKey()), numbers.get(0), numbers.get(1));
    }
    else if (criteria.getOperation() == FilterOperation.CONTAINING) {
      if (root.get(criteria.getKey()).getJavaType() == String.class) {
        return builder.like(
            root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      } else {
        return builder.equal(root.get(criteria.getKey()), criteria.getValue());
      }
    }
    return null;
  }

  private List<BigDecimal> castObjectToListOfBigDecimal(Object obj) {
    List<?> list = new ArrayList<>();
    if (obj.getClass().isArray()) {
      list = Arrays.asList((Object[])obj);
    }
    List<BigDecimal> numbers = new ArrayList<>();
    list.forEach(o -> numbers.add(BigDecimal.valueOf(Double.parseDouble((String) o))));

    return numbers;
  }
}
