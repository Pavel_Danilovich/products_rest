package rest.service;

public enum FilterOperation {
  GREATER(">"),
  LESS("<"),
  GREATER_AND_EQUAL(">="),
  LESS_AND_EQUAL("<="),
  BETWEEN("[]"),
  CONTAINING(":");

  private final String operation;

  FilterOperation(String operation) {
    this.operation = operation;
  }

  public String getOperation() {
    return operation;
  }
}
