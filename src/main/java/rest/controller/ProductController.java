package rest.controller;

import java.util.HashMap;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import rest.entity.Product;
import rest.service.ProductService;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/product")
public class ProductController {
  private final ProductService productService;

  @GetMapping
  public ResponseEntity<List<Product>> getAllProducts(
      @RequestParam(required = false) String sort) {
    return ResponseEntity.ok(productService.findAllProductsSorted(sort));
  }

  @PostMapping("/filter")
  public ResponseEntity<List<Product>> getAllProductsFiltered(
      @RequestBody HashMap<String, String> filter) {
    return ResponseEntity.ok(productService.findAllProductsFiltered(filter));
  }

  @PostMapping
  public ResponseEntity<Product> createProduct(@RequestBody Product product) {
    return ResponseEntity.ok(productService.save(product));

  }

  @DeleteMapping
  @ResponseStatus(value = HttpStatus.OK)
  public void deleteProduct(@RequestBody Product product) {
    productService.delete(product);
  }

  @PutMapping
  public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
    return ResponseEntity.ok(productService.update(product));
  }
}
