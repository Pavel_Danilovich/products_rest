package rest.controller;

import java.util.HashMap;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import rest.entity.Article;
import rest.service.ArticleService;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/article")
public class ArticleController {
  private final ArticleService articleService;

  @GetMapping
  public ResponseEntity<List<Article>> getAllArticles(
      @RequestParam(required = false) String sort) {
    return ResponseEntity.ok(articleService.findAllProductsSorted(sort));
  }

  @PostMapping("/filter")
  public ResponseEntity<List<Article>> getAllArticlesFiltered(
      @RequestBody HashMap<String, String> filter) {
    return ResponseEntity.ok(articleService.findAllProductsFiltered(filter));
  }

  @PostMapping
  public ResponseEntity<Article> createArticle(@RequestBody Article article) {
    return ResponseEntity.ok(articleService.save(article));
  }

  @DeleteMapping
  @ResponseStatus(value = HttpStatus.OK)
  public void deleteArticle(@RequestBody Article article) {
    articleService.delete(article);
  }

  @PutMapping
  public ResponseEntity<Article> updateArticle(@RequestBody Article article) {
    return ResponseEntity.ok(articleService.update(article));
  }
}
