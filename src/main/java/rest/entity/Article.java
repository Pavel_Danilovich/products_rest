package rest.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.*;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Data
@Table(name = "articles")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String contentText;
    private Instant creationDate;
    @ManyToOne
    @JoinColumn(name = "product_id")
    @NotNull
    private Product product;
}
